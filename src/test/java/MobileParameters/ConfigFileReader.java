package MobileParameters;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class ConfigFileReader {

    private Properties properties;
    private final String propertyFilePath= "config//configuration.properties";

    public ConfigFileReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }
    
    public String getDriverType() {
    	String driverType = properties.getProperty("DRIVER_TYPE");
    	if (driverType != null) return driverType;
    	else throw new RuntimeException("Driver type not specified in the Configuration.properties file");
    }
    
    public String getScreenshotPath() {
    	String path = properties.getProperty("SCREEN_SHOT_PATH");
    	if (path != null) return path;
    	else throw new RuntimeException("Screenshot path not specified in the Configuration.properties file.");
    }
    
    public String getPlatFormName() {
    	String platform_name = properties.getProperty("PLATFORM");
    	if (platform_name != null) return platform_name;
    	else throw new RuntimeException("Platform Name not specified in the Configuration.properties file.");
    }

    public String getDeviceName() {
    	String device_name = properties.getProperty("DEVICE_NANE");
    	if (device_name != null) return device_name;
    	else throw new RuntimeException("Device Name not specified in the Configuration.properties file.");
    }

    public String getAppPackage() {
    	String app_package = properties.getProperty("APP_PACKAGE");
    	if (app_package != null) return app_package;
    	else throw new RuntimeException("App Package not specified in the Configuration.properties file.");
    }

    public String getAppActivity() {
    	String app_activity = properties.getProperty("APP_ACTIVITY");
    	if (app_activity != null) return app_activity;
    	else throw new RuntimeException("App Activity not specified in the Configuration.properties file.");
    }
    
    public String getAppiumUrl() {
    	String appium_url = properties.getProperty("APPIUM_URL");
    	if (appium_url != null) return appium_url;
    	else throw new RuntimeException("Appium Url not specified in the Configuration.properties file.");
    }
}
