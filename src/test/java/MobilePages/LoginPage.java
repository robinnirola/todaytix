package MobilePages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LoginPage {
	AppiumDriver<MobileElement> driver;
	
	public LoginPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
	}
	
	public MobileElement email_input() {
		return this.driver.findElementsByClassName("android.widget.EditText").get(0);
	}
	
	public MobileElement password_input() {
		return this.driver.findElementsByClassName("android.widget.EditText").get(1);
	}
	
	public MobileElement login_btn() {
		return this.driver.findElementById("com.todaytix.TodayTix:id/action_button");
	}
}
