package MobilePages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class JoinPage {
	AppiumDriver<MobileElement> driver;
	WebDriverWait wait;
	
	public JoinPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);
	}
	
	public MobileElement title_ele() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/title")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/title");
	}
	
	public MobileElement to_login_btn() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/primary_button_1")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/primary_button_1");
	}
	
	public MobileElement to_register_btn() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/primary_button_2")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/primary_button_2");
	}
	
	public MobileElement continue_btn_without_sign() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.todaytix.TodayTix:id/secondary_button")));
		return this.driver.findElementById("com.todaytix.TodayTix:id/secondary_button");
	}
}
