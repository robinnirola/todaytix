package WebParameters;


import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider(name="ticket name")
    public Object[][] getNorthAmericaCountry() {
        List<String> ticket_name = new ArrayList<String>();
        ticket_name.add("TINA");
        return new Object[][] {{ ticket_name }};
    }


}
