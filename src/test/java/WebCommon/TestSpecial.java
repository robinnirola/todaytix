package WebCommon;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import WebParameters.ConfigFileReader;

import org.apache.commons.io.FileUtils;

public class TestSpecial {
	WebDriver driver;
	
	public TestSpecial(WebDriver driver) {
		this.driver = driver;
	}
	
	public void getWebScreenshot() throws Exception {
		ConfigFileReader conf_file_reader = new ConfigFileReader();
		// Check if file exist or not in screenshot
		File f = new File(conf_file_reader.getScreenshotPath());
		if(f.exists() && !f.isDirectory()) { 
		    FileUtils.delete(f);
		}
		// Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot =((TakesScreenshot)this.driver);
		// Call getScreenshotAs method to create image file
		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		// Move image file to new destination
		System.out.println(System.getProperty("user.dir"));
		File DestFile=new File(System.getProperty("user.dir") + conf_file_reader.getScreenshotPath());
		// Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);
	}
}
