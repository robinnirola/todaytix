package WebCommon;

import java.util.ArrayList;
import java.util.List;


import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ImgProcess {
	public ImgProcess() {
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );		
	}
	
	public Mat readImgByPath(String path) {
		Mat img = Imgcodecs.imread(path);
		return img;
	}
    
    public Mat convertRGBToHSV(Mat img) {
    	// remove some noise and convert image to hsv color space
    	Imgproc.blur(img, img, new Size(7, 7));
    	Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2HSV);
    	return img;
    }
    
//    min_val: [40, 21, 87]
//    max_val: [100, 255, 255]
    public void detectObjectByColorRange(Mat img, Scalar min_val, Scalar max_val) {
    	Core.inRange(img, min_val, max_val, img);    	
    }
    
    public Mat removeNoise(Mat img) {
    	Mat kernel = new Mat(new Size(3, 3), CvType.CV_8UC1, new Scalar(255));
		Imgproc.morphologyEx(img, img, Imgproc.MORPH_OPEN, kernel);
        Imgproc.morphologyEx(img, img, Imgproc.MORPH_CLOSE, kernel);
        return img;
    }
    
    public List<MatOfPoint> findingContoursFromImage(Mat img) {
    	//Finding Contours
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchey = new Mat();
        Imgproc.findContours(img, contours, hierarchey, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        //Drawing the Contours
        Scalar color = new Scalar(0, 0, 255);
        Imgproc.drawContours(img, contours, -1, color, 2, Imgproc.LINE_8, hierarchey, 2, new Point() ) ;
        return contours;
    }
    
    public MatOfPoint findingLargestAreaContour(List<MatOfPoint> contours) {
    	MatOfPoint temp_contour = contours.get(0); //the largest is at the index 0 for starting point
        MatOfPoint largest_contour = contours.get(0);
        for(int index = 0; index < contours.size(); index++) {
        	Rect dd = Imgproc.boundingRect(contours.get(index));
        	System.out.println(dd.x + ":" + dd.y);
        	temp_contour = contours.get(index);        	
        	double contourarea = Imgproc.contourArea(temp_contour);
        	if (contourarea > Imgproc.contourArea(largest_contour)) {
                largest_contour = temp_contour;
            }
        }
        return largest_contour;
    }
}
