package WebCommon;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import MobileCommon.CommonAction;
import WebParameters.ConfigFileReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseTest {
    public WebDriver driver1;
    public AppiumDriver<MobileElement> driver2;
    public CommonAction common_action;

    public BaseTest() {
        this.setupDriver();
        this.common_action = new CommonAction(this.driver2);
    }

    public void setupDriver() {
        ConfigFileReader configFileReader = new ConfigFileReader();
        System.out.println(configFileReader.getDriverType().toLowerCase());
        System.out.println(configFileReader.getDriverType().toLowerCase() == "chrome");
        if (configFileReader.getDriverType().toLowerCase().contains("chrome")) {
        	System.out.println("chrome driver");
        	// Set driver option and Initialize web driver
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + configFileReader.getDriverPath());
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--incognito");
            options.setExperimentalOption("useAutomationExtension", false);
            this.driver1 = new ChromeDriver(options);
            this.driver1.manage().window().maximize();
            this.driver1.get(configFileReader.getApplicationUrl());
        }else {
        	try {
        		DesiredCapabilities caps = new DesiredCapabilities();
    			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
    			caps.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
    			caps.setCapability("appPackage", "com.todaytix.TodayTix");
    			caps.setCapability("appActivity", "com.todaytix.TodayTix.activity.MainActivity");
    			
    			URL url = new URL("http://127.0.0.1:4723/wd/hub");
//    			this.driver = new AppiumDriver<MobileElement>(url, caps);
    			this.driver2 = new AndroidDriver<MobileElement>(url, caps);
//    			this.driver2 = new IOSDriver<MobileElement>(url, caps);
    			this.driver2.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        	}catch(Exception e) {
        		System.out.println(e.getMessage());
        	}
        }

        
    }

}
