package WebEmulateAction;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JsActions {
    WebDriver driver;

    public JsActions(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement visibleElement(WebElement ele) {
        String js = "arguments[0].style.visibility='visible';";
        ((JavascriptExecutor) this.driver).executeScript(js, ele);
        return ele;
    }
}
