package WebPages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {
    WebDriver driver;
    WebDriverWait wait;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 5);
    }

    public WebElement search_input() {
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-input-widget input")));
        return this.driver.findElement(By.cssSelector("#search-input-widget input"));
    }
    
    public WebElement search_submit_btn() {
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-input-widget button")));
    	return this.driver.findElement(By.cssSelector("#search-input-widget button"));
    }
    
    public WebElement search_recommend_ul() {
    	this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#search-service ul.c-quick-search__list")));
    	return this.driver.findElement(By.cssSelector("#search-service ul.c-quick-search__list"));
    }
}
