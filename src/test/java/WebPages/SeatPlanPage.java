package WebPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeatPlanPage {
	WebDriver driver;
	WebDriverWait wait;
	
	public SeatPlanPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 5);
	}
	
	public WebElement add_basket_btn() {
//		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.seat-summary button")));
		return this.driver.findElement(By.cssSelector("div.seat-summary button"));
	}
}
