package WebPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingCalendarPage {
	WebDriver driver;
	WebDriverWait wait;
	
	public BookingCalendarPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);
	}
	
	public List<WebElement> enable_date_li() throws InterruptedException {
		Thread.sleep(10000);
		this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector(".dayContainer span.flatpickr-day")));
		return this.driver.findElements(By.cssSelector(".dayContainer span.t-affiliate-font-color"));
	}
	
	public WebElement sub_ticket() {
		List<WebElement> sub_ticket_li = this.driver.findElements(By.cssSelector(".full-fat-calendar label"));
		return sub_ticket_li.get(0);
	}
	
	public WebElement pick_seat_btn() {
		return this.driver.findElement(By.cssSelector(".full-fat-calendar button"));
	}
}
