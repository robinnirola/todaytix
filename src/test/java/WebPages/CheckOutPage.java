package WebPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckOutPage {
	WebDriver driver;
	WebDriverWait wait;
	
	public CheckOutPage(WebDriver driver) {
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, 10);
	}
	
	public WebElement checkout_title() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h1")));
		return this.driver.findElement(By.cssSelector("h1"));
	}
	
	public WebElement first_ticket() {
		WebElement ticket_ul = this.driver.findElements(By.cssSelector("ul")).get(0);
		WebElement first_ticket = ticket_ul.findElements(By.cssSelector("li")).get(0);
		return first_ticket;
	}
	
	public WebElement remove_ticket_alert() {
		this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.o-alert")));
		return this.driver.findElement(By.cssSelector("div.o-alert"));
	}
	
	
}
