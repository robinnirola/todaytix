package MobileCommon;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import MobileParameters.ConfigFileReader;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseTest {
	public DesiredCapabilities capabilities;
	public AppiumDriver<MobileElement> driver;
	public CommonAction common_action;
	
	public BaseTest() {
        this.setupDriver();
		this.common_action = new CommonAction(this.driver);
    }
	
	public void setupDriver() {
		try {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
			caps.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
			caps.setCapability("appPackage", "com.todaytix.TodayTix");
			caps.setCapability("appActivity", "com.todaytix.TodayTix.activity.MainActivity");
			
			URL url = new URL("http://127.0.0.1:4723/wd/hub");
//			this.driver = new AppiumDriver<MobileElement>(url, caps);
			this.driver = new AndroidDriver<MobileElement>(url, caps);
//			this.driver = new IOSDriver<MobileElement>(url, caps);
			this.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
