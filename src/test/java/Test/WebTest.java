package Test;

import java.util.Iterator;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.HighGui;
import org.opencv.imgproc.Imgproc;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import WebCommon.BaseTest;
import WebCommon.ImgProcess;
import WebCommon.TestSpecial;
import WebPages.BookingCalendarPage;
import WebPages.CheckOutPage;
import WebPages.MainPage;
import WebPages.SeatPlanPage;
import WebParameters.ConfigFileReader;
import WebParameters.DataProviderClass;

public class WebTest extends BaseTest{
	public Scalar min_val;
	public Scalar max_val;
	
	@Test(priority = 1, groups = {"Search Ticket", "Checkout Journey", "Checkout Journey For Restricted View"}, dataProvider = "ticket name", dataProviderClass = DataProviderClass.class)
    public void searchTicket(List<String> ticket_name) throws InterruptedException {
        // get items that has two options for language
        MainPage main_page = new MainPage(this.driver1);
        main_page.search_input().sendKeys(ticket_name.get(0));
        System.out.println(ticket_name.get(0));
        main_page.search_recommend_ul().findElements(By.cssSelector("li")).get(0).click();
        main_page.search_submit_btn().click();
    }
	
	@Test(priority = 2, groups = {"Checkout Journey"})
	public void bookingCalendarForCheckoutJourney() throws Exception {
		// Booking page
		BookingCalendarPage calendar_page = new BookingCalendarPage(this.driver1);
		calendar_page.enable_date_li().get(1).click();
		calendar_page.sub_ticket().click();
		calendar_page.pick_seat_btn().click();		
		Thread.sleep(10000);
		
		// yellow + red
		this.min_val = new Scalar(0, 40, 87);
		this.max_val = new Scalar(50, 255, 255);
	}
	
	@Test(priority = 3, groups = {"Checkout Journey For Restricted View"})
	public void bookingCalendarForCheckoutJourneyForRestrict() throws Exception {
		// Booking page
		BookingCalendarPage calendar_page = new BookingCalendarPage(this.driver1);
		calendar_page.enable_date_li().get(5).click();
		calendar_page.sub_ticket().click();
		calendar_page.pick_seat_btn().click();		
		Thread.sleep(10000);
		
		// red
		this.min_val = new Scalar(0, 40, 87);
		this.max_val = new Scalar(20, 255, 255);
	}
	
	
	@Test(priority = 4, groups = {"Select Seats", "Checkout Journey", "Checkout Journey For Restricted View"})
	public void	selectSeats() throws Exception {		
		// Seat Plan paged
		ConfigFileReader conf_file_reader = new ConfigFileReader();
		TestSpecial test_spec = new TestSpecial(this.driver1);
		test_spec.getWebScreenshot();
		ImgProcess img_proc = new ImgProcess();
		Mat img = img_proc.readImgByPath(System.getProperty("user.dir") + conf_file_reader.getScreenshotPath());
		img = img_proc.convertRGBToHSV(img);
		
		// min_val: [0, 40, 87], max_val: [50, 255, 255] : red + yellow
		// min_val: [0, 40, 87], max_val: [20, 250, 250] : red
		// min_val: [20, 40, 87], max_val: [30, 250, 250] : yellow		
//		this.min_val = new Scalar(0, 40, 87);
//		this.max_val = new Scalar(50, 255, 255);
		img_proc.detectObjectByColorRange(img, this.min_val, this.max_val);
		img = img_proc.removeNoise(img);
        List<MatOfPoint> contours = img_proc.findingContoursFromImage(img);        
        Iterator<MatOfPoint> it = contours.iterator();
        while (it.hasNext()) {
           MatOfPoint i = it.next();
           Rect rect = Imgproc.boundingRect(i);
           if (rect.y < 200) {
        	   it.remove();
           }
        }
        
        MatOfPoint largest_contour = img_proc.findingLargestAreaContour(contours);
        Rect largest_rect = Imgproc.boundingRect(largest_contour);
        System.out.println(largest_rect.x + ": " + largest_rect.y + ", area=" + largest_rect.width + " * " + largest_rect.height);
        
        Actions action = new Actions(this.driver1);
		action.moveByOffset(largest_rect.x, largest_rect.y).click().click().build().perform();

		SeatPlanPage seat_plan_page = new SeatPlanPage(this.driver1);
		for(int index = 1; index < 1000; index = index + 2) {
			System.out.println(index);
			action.moveByOffset(index, index).click().build().perform();
			Thread.sleep(1000);
			try {
				seat_plan_page.add_basket_btn().click();
			}catch(Exception e) {
				continue;
			}
			break;
		}
		
		Thread.sleep(3000);
		
		// checkout page
		CheckOutPage checkout_page = new CheckOutPage(this.driver1);
		if(checkout_page.checkout_title().getText().contains("BASKET")) {
			String ticket_name = checkout_page.first_ticket().findElement(By.cssSelector("h5")).getText();
			System.out.println(ticket_name);
			checkout_page.first_ticket().findElement(By.cssSelector("button")).click();
			Thread.sleep(3000);
			checkout_page.remove_ticket_alert().findElement(By.cssSelector("button.js-remove")).click();
			Thread.sleep(3000);
		}
//		HighGui.imshow("eden hazard", img);
//    	HighGui.waitKey();
		
	}
	
	@AfterTest(alwaysRun = true)
    public void deleteDriver() {
        this.driver1.quit();
    }
	
}
