package Main;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.HighGui;
import org.opencv.imgproc.Imgproc;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat mat = Mat.eye(3, 3, CvType.CV_8UC1);
		System.out.println("mat=" + mat.dump());
		
		ConfigFileReader conf_file_reader = new ConfigFileReader();
		ImgProcess img_proc = new ImgProcess();
		Mat img = img_proc.readImgByPath(System.getProperty("user.dir") + conf_file_reader.getScreenshotPath());
		img = img_proc.convertRGBToHSV(img);
		Scalar min_val = new Scalar(120, 40, 87);
		Scalar max_val = new Scalar(170, 250, 250);
		img_proc.detectObjectByColorRange(img, min_val, max_val);
		img = img_proc.removeNoise(img);        
		HighGui.imshow("eden hazard2", img);
    	HighGui.waitKey();
    	List<MatOfPoint> contours = img_proc.findingContoursFromImage(img);
        for(int index = 0; index < contours.size(); index++) {
        	Rect rect = Imgproc.boundingRect(contours.get(index));
        	if (rect.y < 200) {
        		contours.remove(index);
        	}
        }
        MatOfPoint largest_contour = img_proc.findingLargestAreaContour(contours);
        Rect largest_rect = Imgproc.boundingRect(largest_contour);
        System.out.println(largest_rect.x + ": " + largest_rect.y + ", area=" + largest_rect.width + " * " + largest_rect.height);
        
	}

}
